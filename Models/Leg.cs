using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Kore
{
    [Table("leg")]
    public class Leg
    {
        [Column("id")]
        public int Id { get; set; }

        [Column("from_stand")]
        public int FromStand { get; set; }

        [Column("to_stand")]
        public int ToStand { get; set; }

        [Column("place")]
        public int Place { get; set; }

        [Column("distance")]
        public int Distance { get; set; }

        [JsonConverter(typeof(JsonStringEnumConverter))]
        [Column("status")]
        public RouteStatus Status { get; set; }

        [Column("started")]
        public DateTime Started { get; set; }

        [Column("completed")]
        public DateTime Completed { get; set; }

        public ICollection<Order>? Orders { get; set; }

        [JsonIgnore]
        [ForeignKey("route_id")]
        public Route Route { get; set; }

    }
}