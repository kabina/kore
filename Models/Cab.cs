using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Kore
{
    [Table("cab")]
    public class Cab
    {
        [Column("id")]
        public int Id { get; set; }

        [Column("name")]
        public string? Name { get; set; }

        [Column("location")]
        public int Location { get; set; }

        [JsonConverter(typeof(JsonStringEnumConverter))]
        [Column("status")]
        public CabStatus Status { get; set; }

        [JsonIgnore]
        public ICollection<Order>? Orders { get; set; }
    }

    public enum CabStatus {
        ASSIGNED,
        FREE,
        CHARGING, // out of order, ...
    }
}