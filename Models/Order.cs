using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using System.ComponentModel.DataAnnotations;

namespace Kore
{
    [Table("taxi_order")]
    public class Order
    {
        [Key]  //
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("id")]
        public int Id { get; set; }

        [Column("from_stand")]
        public int FromStand { get; set; }

        [Column("to_stand")]
        public int ToStand { get; set; }

        [Column("max_wait")]
        public int MaxWait { get; set; }

        [Column("max_loss")]
        public int MaxLoss { get; set; }

        [Column("eta")]
        public int? Eta { get; set; }

        [JsonConverter(typeof(JsonStringEnumConverter))]
        [Column("status")]
        public OrderStatus? Status { get; set; }

        [Column("shared")]
        public bool Shared { get; set; }

        [Column("in_pool")]
        public bool inPool { get; set; }

        [Column("distance")]
        public int? Distance { get; set; }

        [Column("received")]
        public DateTime? Received { get; set; }

        [Column("started")]
        public DateTime? Started { get; set; }

        [Column("completed")]
        public DateTime? Completed { get; set; }

        [ForeignKey("cab_id")]
        public Cab? Cab { get; set; }

        [ForeignKey("customer_id")]
        public Customer? Customer { get; set; }

        [ForeignKey("leg_id")]
        public Leg? Leg { get; set; }

        [ForeignKey("route_id")]
        public Route? Route { get; set; }
    }

    public enum OrderStatus
    {
        RECEIVED,  // sent by customer
        ASSIGNED,  // assigned to a cab, a proposal sent to customer with time-of-arrival
        ACCEPTED,  // plan accepted by customer, waiting for the cab
        CANCELLED, // cancelled by customer before assignment
        REJECTED,  // proposal rejected by customer
        ABANDONED, // cancelled after assignment but before 'PICKEDUP'
        REFUSED,   // no cab available, cab broke down at any stage
        PICKEDUP,
        COMPLETED
    }
}