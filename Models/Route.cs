using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Kore
{
    [Table("route")]
    public class Route
    {
        [Column("id")]
        public int Id { get; set; }

        [JsonConverter(typeof(JsonStringEnumConverter))]
        [Column("status")]
        public RouteStatus Status { get; set; }

        public ICollection<Leg>? Legs { get; set; }
        
        public ICollection<Order>? Orders { get; set; }

        [ForeignKey("cab_id")]
        public Cab Cab { get; set; }
    }

    public enum RouteStatus
    {
        PLANNED,   // proposed by Pool
        ASSIGNED,  // not confirmed, initial status 
        ACCEPTED,  // plan accepted by customer, waiting for the cab
        REJECTED,  // proposal rejected by customer(s)
        ABANDONED, // cancelled after assignment but before 'PICKEDUP'
        STARTED,   // status needed by legs
        COMPLETED
    }

}