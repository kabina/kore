using System.ComponentModel.DataAnnotations.Schema;

namespace Kore
{
    [Table("customer")]
    public class Customer
    {
        [Column("id")]
        public int Id { get; set; }

        public ICollection<Order>? Orders { get; set; }
    }
}