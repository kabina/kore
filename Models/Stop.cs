using System.ComponentModel.DataAnnotations.Schema;

namespace Kore
{
    [Table("stop")]
    public class Stop
    {
        [Column("id")]
        public int Id { get; set; }

        [Column("no")] // some number
        public string? No { get; set; }

        [Column("name")]
        public string? Name { get; set; }

        [Column("type")]
        public string? Type { get; set; }

        [Column("bearing")]
        public int Bearing { get; set; }

        [Column("latitude")]
        public double Latitude { get; set; }

        [Column("longitude")]
        public double Longitude { get; set; }
    }
}