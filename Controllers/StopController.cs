using Kore.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Kore.Controllers {
    [Route("stops")]
    [ApiController]
    public class StopController : ControllerBase {

        private readonly ILogger<StopController> _logger;
        private readonly IStopRepository _service;
        private IHttpContextAccessor _httpContextAccessor;

        public StopController(ILogger<StopController> logger,
                                IStopRepository service,
                                IHttpContextAccessor httpContextAccessor) {
            _logger = logger;
            _service = service;
            _httpContextAccessor = httpContextAccessor;
            Utils.InitDistance(service.GetAllStops().ToArray());
        }
        
        [HttpGet(Name = "GetAllStops")]
        [Authorize(Roles = "Admin,Cab,Customer")]
        public List<Stop> GetAll() {
            var usrId = Utils.getUserId(_httpContextAccessor);
            _logger.LogInformation("usr_id=" + usrId);
            return _service.GetAllStops();
        }
    }
}