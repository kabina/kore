using Kore.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Kore.Controllers
{
    [Route("cabs")]
    [ApiController]
    public class CabController : ControllerBase
    {

        private readonly ILogger<CabController> _logger;
        private readonly ICabRepository _cabService;
        private IHttpContextAccessor _httpContextAccessor;

        public CabController(ILogger<CabController> logger, ICabRepository cabService,
                             IHttpContextAccessor httpContextAccessor) {
            _logger = logger;
            _cabService = cabService;
            _httpContextAccessor = httpContextAccessor;
        }
        
        [HttpGet(Name = "GetOneCab")]
        [Route("{id:int}")] // https://localhost:7128/cabs/1
        [Authorize(Roles = "Admin,Cab,Customer")]
        public async Task<Cab> Get(int id) {
            //HttpContext.User.Claims.ToList();
            //HttpContext.User?.IsInRole("Admin");
            string userId = Utils.getUserId(_httpContextAccessor);
            _logger.LogInformation("GET cab_id={0} usr_id={1}", id, userId);
            return await _cabService.GetCabByIdAsync(id);
        }

        [HttpPost(Name = "InsertCab")]
        [Authorize(Roles = "Cab")]
        public async Task<Cab?> Post() {
            Cab? cab = await Utils.ReadFromBodyAsync<Cab>(Request.Body);
            if (cab == null) return null;
            return await _cabService.AddCabAsync(cab);
        }

        /*[HttpPut(Name = "UpdateCab")]
        [Route("{id}")] // we don't really need id, as we can read from body object
        [Authorize(Roles = "Cab")]
        public async Task<ActionResult<Cab>> Put(string id, Cab cab) {
            string usrId = getUserId(_httpContextAccessor);
            if (usrId != id) {
                // simplification - cab ID and driver ID should be the same
                return StatusCode(401, "Not authorized");
            }
            Cab c = await _cabService.ModifyCabAsync(cab);
            return c;
        }
        */

        //curl -u "cab721:ca" -X PUT -d '{"location":1,"status":1}' http://localhost:5128/cabs
        [HttpPut(Name = "UpdateCab")]
        //[Route("{id}")] // we don't really need id, as we can read from body object
        [Authorize(Roles = "Cab")]
        public async Task<Cab?> Put() // [FromBody] string content
        {
            Cab? dto = await Utils.ReadFromBodyAsync<Cab>(Request.Body);
           
            string? usrId = Utils.getUserId(_httpContextAccessor);
            if (dto == null || usrId == null) {
                _logger.LogInformation("PUT cab: NULL");
                return null;
            }
            _logger.LogInformation("PUT cab_id={0} location={1}, status={2}",
                                    usrId, dto.Location, dto.Status);
            Cab cab = await _cabService.GetPureCabByIdAsync(Int32.Parse(usrId));
            if (cab == null) {
                // simplification - cab ID and driver ID should be the same
                _logger.LogInformation("PUT: no cab found, cab_id={0} location={1}, status={2}",
                                    usrId, dto.Location, dto.Status);
                return null; //Task<ActionResult... StatusCode(401, "Not authorized"); 
            }
            cab.Location = dto.Location;
            cab.Status = dto.Status;
            return await _cabService.ModifyCabAsync(cab);
        }
    }
}