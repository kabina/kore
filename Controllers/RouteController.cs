using Kore.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Kore.Controllers {
    [Route("routes")]
    [ApiController]
    public class RouteController : ControllerBase {

        private readonly ILogger<RouteController> _logger;
        private readonly IRouteRepository _service;
        private IHttpContextAccessor _httpContextAccessor;

        public RouteController(ILogger<RouteController> logger,
                                IRouteRepository service,
                                IHttpContextAccessor httpContextAccessor) {
            _logger = logger;
            _service = service;
            _httpContextAccessor = httpContextAccessor;
        }
        
        [HttpGet(Name = "GetMyRoutes")]
        [Authorize(Roles = "Admin,Cab")]
        public async Task<Route?> Get() {
            string? usrId = Utils.getUserId(_httpContextAccessor);
            if (usrId == null) {
                _logger.LogInformation("GET cab: NULL");
                return null;
            }
            _logger.LogInformation("GET cab_id=" + usrId);
            return await _service.GetRouteByCabAsync(usrId);
        }

        [HttpPut(Name = "UpdateRoute")] // well, status only
        [Authorize(Roles = "Cab")]
        public async Task<Route?> Put() {
            string? usrId = Utils.getUserId(_httpContextAccessor);
            Route? dto = await Utils.ReadFromBodyAsync<Route>(Request.Body);
            if (usrId == null || dto == null) {
                _logger.LogInformation("PUT route, dto=NULL cust_id={0}", usrId);
                return null;
            }
            _logger.LogInformation("PUT route cust_id={0}, route_id={1}, status={2}",
                                    usrId, dto.Id, dto.Status);
            Route? route = await _service.GetRouteByIdAsync(dto.Id);
            if (route == null) {
                _logger.LogInformation("PUT route getRoute=NULL cust_id={0}, route_id={1}",
                                       usrId, dto.Id);
                return null;
            }
            // authorisation
            if (usrId != route.Cab.Id.ToString())
            {
                _logger.LogInformation("PUT route unathorised cust_id={0}, route_id={1}, cab_id={2}",
                                        usrId, dto.Id, route.Cab.Id);
                return null; //Task<ActionResult... StatusCode(401, "Not authorized"); 
            }
            route.Status = dto.Status;
            return await _service.ModifyRouteAsync(route);
        }
    }
}