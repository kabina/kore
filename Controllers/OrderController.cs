using Kore.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Kore.Controllers
{
    [Route("orders")]
    [ApiController]
    public class OrderController : ControllerBase
    {

        private readonly ILogger<OrderController> _logger;
        private readonly IOrderRepository _orderService;
        private readonly ICustomerRepository _custService;
        private IHttpContextAccessor _httpContextAccessor;

        public OrderController(ILogger<OrderController> logger,
                                IOrderRepository orderService,
                                ICustomerRepository custService,
                                IHttpContextAccessor httpContextAccessor) {
            _logger = logger;
            _orderService = orderService;
            _custService = custService;
            _httpContextAccessor = httpContextAccessor;
        }
        
        [HttpGet(Name = "GetOneOrder")]
        [Route("{id:int}")] // https://localhost:7128/cabs/1
        [Authorize(Roles = "Admin,Customer")]
        public async Task<Order?> Get(int id)
        {
            string? userId = Utils.getUserId(_httpContextAccessor);
            _logger.LogInformation("GET order_id={0} cust_id={1}", id, userId);
            Order? ord = await _orderService.GetOrderByIdAsync(id);
            if (ord == null || ord.Customer == null || ord.Customer.Id.ToString() != userId) {
                _logger.LogInformation("Customer {0} not allowed to see order_id={1}",
                                        userId, id);
                return null;
            }
            return ord;
        }

        // curl -X PUT -u "cust28:cust28" -d '{"id":727232, "status":"RECEIVED"}' https://localhost:7128/orders
        [HttpPut(Name = "UpdateOrder")] // well, status only
        [Authorize(Roles = "Customer")]
        public async Task<Order?> Put() {
            string? usrId = Utils.getUserId(_httpContextAccessor);
            Order? dto = await Utils.ReadFromBodyAsync<Order>(Request.Body);
            if (usrId == null || dto == null) {
                _logger.LogInformation("PUT order: NULL");
                return null;
            }
            _logger.LogInformation("PUT order cust_id={0}, order_id={1}, status={2}",
                                    usrId, dto.Id, dto.Status);
            Order? ord = await _orderService.GetOrderByIdAsync(dto.Id);
            // authorisation
            if (usrId != ord?.Customer?.Id.ToString()) {
                return null; //Task<ActionResult... StatusCode(401, "Not authorized"); 
            }
            ord.Status = dto.Status;
            return await _orderService.ModifyOrderAsync(ord);
        }

        //curl -X POST -u "cust28:cust28" -d '{"id":-1, "fromStand":4082, "toSTand":"4083", "maxWait":10, "maxLoss":90, "shared": true}' https://localhost:7128/orders
        [HttpPost(Name = "InsertOrder")]
        [Authorize(Roles = "Customer")]
        public async Task<Order?> Post() {
            string? usrId = Utils.getUserId(_httpContextAccessor);
            Order? o = await Utils.ReadFromBodyAsync<Order>(Request.Body);
            if (usrId == null || o == null) {
                _logger.LogInformation("POST order: NULL");
                return null;
            }
            _logger.LogInformation("POST order cust_id={0}, from={1}, to={2}",
                                    usrId, o.FromStand, o.ToStand);
            Customer c = await _custService.GetPureCustomerByIdAsync(usrId);
            if (c == null) {
                _logger.LogInformation("POST order: customer not found in DB, cust_id={0}", usrId);
                return null;
            }
            return await _orderService.AddOrderAsync(o, c);
        }
    }
}