using Kore.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Kore.Controllers {
    [Route("legs")]
    [ApiController]
    public class LegController : ControllerBase {

        private readonly ILogger<LegController> _logger;
        private readonly ILegRepository _service;
        private IHttpContextAccessor _httpContextAccessor;

        public LegController(ILogger<LegController> logger,
                                ILegRepository service,
                                IHttpContextAccessor httpContextAccessor) {
            _logger = logger;
            _service = service;
            _httpContextAccessor = httpContextAccessor;
        }
    
        [HttpPut(Name = "UpdateLeg")] // well, status only
        [Authorize(Roles = "Cab")]
        public async Task<Leg?> Put() {
            string? usrId = Utils.getUserId(_httpContextAccessor);
            Leg? dto = await Utils.ReadFromBodyAsync<Leg>(Request.Body);
            if (usrId == null || dto == null) {
                _logger.LogInformation("PUT Leg: NULL");
                return null;
            }
            _logger.LogInformation("PUT Leg cust_id={0}, leg_id={1}, status={2}",
                                    usrId, dto.Id, dto.Status);
            Leg? leg = await _service.GetLegByIdAsync(dto.Id);
            if (leg == null || leg.Route == null || leg.Route.Cab == null) {
                _logger.LogInformation("PUT leg NULL cust_id={0}, leg_id={1}",
                                       usrId, dto.Id);
                return null;
            }
            // authorisation
            if (usrId != leg.Route.Cab.Id.ToString())
            {
                _logger.LogInformation("PUT leg unathorised cust_id={0}, leg_id={1}, cab_id={2}",
                                        usrId, dto.Id, leg.Route.Cab.Id);
                return null; //Task<ActionResult... StatusCode(401, "Not authorized"); 
            }
            leg.Status = dto.Status;
            return await _service.ModifyLegAsync(leg);
        }
    }
}