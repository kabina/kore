﻿using Microsoft.EntityFrameworkCore;

namespace Kore
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; set; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<KoreContext>(options =>
                    options.UseNpgsql(Configuration.GetConnectionString("KoreContext")));
        }
    }
}
