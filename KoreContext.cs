﻿using Microsoft.EntityFrameworkCore;

namespace Kore {
    public class KoreContext : DbContext {
        //public KoreContext(DbContextOptions<KoreContext> options) : base(options) { }

        public DbSet<Cab> Cabs { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Leg> Legs { get; set; }
        public DbSet<Route> Routes { get; set; }
        public DbSet<Stop> Stops { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseNpgsql("Host=192.168.10.176;Port=5432;Database=kabina;Username=kabina;Password=kaboot;Pooling=true;MinPoolSize=1;MaxPoolSize=1000");
            // System.InvalidCastException: Cannot write DateTime with Kind=Unspecified to PostgreSQL type 'timestamp with time zone', only UTC is supported
            AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
            AppContext.SetSwitch("Npgsql.DisableDateTimeInfinityConversions", true);
        }
        /*
        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Order>(e => e.ToTable("taxi_order").Property(p => p.Id).UseIdentityAlwaysColumn()); // .ValueGeneratedOnAdd()
            //modelBuilder.Entity<Order>().Property(o => o.Id).HasDefaultValueSql("nextval('\"taxi_order\"')");
            modelBuilder.Entity<Customer>(e => e.ToTable("customer"));
            modelBuilder.Entity<Cab>(e => e.ToTable("cab"));
            modelBuilder.Entity<Leg>(e => e.ToTable("leg"));
            modelBuilder.Entity<Route>(e => e.ToTable("route"));
            modelBuilder.Entity<Stop>(e => e.ToTable("stop"));
        }
        */
        
    }
}
