﻿using Microsoft.AspNetCore.Authentication;

namespace Kore
{
    public class BasicAuthenticationOptions : AuthenticationSchemeOptions
    {
        public string Realm { get; set; }
    }
}
