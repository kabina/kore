﻿using Microsoft.EntityFrameworkCore;

namespace Kore.Services
{
    public class CustomerRepository : ICustomerRepository
    {
        public async Task<Customer> GetCustomerByIdAsync(string id) {
            var _db = new KoreContext();
            return await _db.Customers.Where(c => c.Id == Int32.Parse(id))
                    .Include(r => r.Orders)
                    .FirstAsync();
        }

        public async Task<Customer> GetPureCustomerByIdAsync(string id) {
            var _db = new KoreContext();
            return await _db.Customers.Where(c => c.Id == Int32.Parse(id)).FirstAsync();
        }
    }
}
