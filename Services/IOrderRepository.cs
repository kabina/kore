﻿namespace Kore.Services
{
    public interface IOrderRepository
    {
        public Task<List<Order>> GetOrderByCustomerAndStatusAsync(int id, string status);
        public Task<Order?> GetOrderByIdAsync(int id);
        public Task<Order?> AddOrderAsync(Order order, Customer cust);
        public Task<Order> ModifyOrderAsync(Order order);
    }
}
