﻿namespace Kore.Services
{
    public interface ICabRepository
    {
        public Task<Cab> GetCabByIdAsync(int id);
        public Task<Cab> GetPureCabByIdAsync(int id);
        public Task<Cab> AddCabAsync(Cab cab);
        public Task<Cab> ModifyCabAsync(Cab cab);
    }
}
