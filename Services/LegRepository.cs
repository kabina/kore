﻿using Microsoft.EntityFrameworkCore;

namespace Kore.Services
{
    public class LegRepository : ILegRepository
    {
        public async Task<Leg?> GetLegByIdAsync(int id) {
            var _db = new KoreContext();
            return await _db.Legs.Where(l => l.Id == id)
                        .Include(l => l.Route)
                            .ThenInclude(r => r.Cab)
                        .SingleOrDefaultAsync();
        }

        public async Task<Leg> ModifyLegAsync(Leg leg) {
            var _db = new KoreContext();
            _db.Legs.Update(leg);
            await _db.SaveChangesAsync();
            return leg;
        }
    }
}
