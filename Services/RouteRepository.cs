﻿using Microsoft.EntityFrameworkCore;

namespace Kore.Services
{
    public class RouteRepository : IRouteRepository
    {
        public async Task<Route?> GetRouteByCabAsync(string id) {
            using var _db = new KoreContext();
            return await _db.Routes.Where(route => route.Cab.Id == Int32.Parse(id)
                                    && route.Status == RouteStatus.ASSIGNED)
                    .Include(r => r.Cab)
                    .Include(r => r.Legs)
                    .Include(r => r.Orders)
                    .FirstOrDefaultAsync();
        }

        public async Task<Route?> GetRouteByIdAsync(int id){
            using var _db = new KoreContext();
            return await _db.Routes.Where(route => route.Id == id)
                    .Include(r => r.Cab)
                    .SingleOrDefaultAsync();
        }

        public async Task<Route> ModifyRouteAsync(Route route) {
            using var _db = new KoreContext();
            _db.Routes.Update(route);
            await _db.SaveChangesAsync();
            return route;
        }
    }
}
