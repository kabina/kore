﻿namespace Kore.Services
{
    public class StopRepository : IStopRepository
    {
        private readonly KoreContext _db;

        public StopRepository() {
            this._db = new KoreContext();
        }

        public List<Stop> GetAllStops() {
            return (from stop in _db.Stops select stop).ToList();
        }
    }
}
