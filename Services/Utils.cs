﻿using System;
using Newtonsoft.Json;
using System.Text;

namespace Kore.Services
{
    public class Utils
    {
        private const int MAXSTOPS = 6000;

        public static short[,] distance = new short[MAXSTOPS, MAXSTOPS];

        public Utils()
        {
        }


        public static string? getUserId(IHttpContextAccessor accessor)
        {
            return accessor?.HttpContext?.User?.Claims?
                    .First(c => c.Type == "userid")?.Value;
        }

        public static async Task<T?> ReadFromBodyAsync<T>(Stream stream)
        {
            string content;
            using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
            {
                content = await reader.ReadToEndAsync();
            }
            if (content == null) return default(T);
            return JsonConvert.DeserializeObject<T>(content);
            //JavaScriptSerializer serializer = new JavaScriptSerializer();
            //dynamic cab = serializer.Deserialize<Cab>(content);
        }

        // Distance service
        private const double M_PI = 3.14159265358979323846;
        private const double m_pi_180 = M_PI / 180.0;
        private const double rev_m_pi_180 = 180.0 / M_PI;

        static double Deg2rad(double deg) { return (deg * m_pi_180); }
        static double Rad2deg(double rad) { return (rad * rev_m_pi_180); }

        // https://dzone.com/articles/distance-calculation-using-3
        static double Dist(double lat1, double lon1, double lat2, double lon2)
        {
            double theta = lon1 - lon2;
            double dist = Math.Sin(Deg2rad(lat1)) * Math.Sin(Deg2rad(lat2)) + Math.Cos(Deg2rad(lat1))
                          * Math.Cos(Deg2rad(lat2)) * Math.Cos(Deg2rad(theta));
            dist = Math.Acos(dist);
            dist = Rad2deg(dist);
            dist *= 60 * 1.1515;
            dist *= 1.609344;
            return dist;
        }

        
        public static void InitDistance(Stop[] stops)
        {
            for (int i = 0; i < stops.Length; i++) {
                distance[i, i] = 0;
                for (int j = i + 1; j < stops.Length; j++) {
                    double d = Dist(stops[i].Latitude, stops[i].Longitude, stops[j].Latitude, stops[j].Longitude);
                    distance[stops[i].Id, stops[j].Id] = (short)d; // TASK: we might need a better precision - meters/seconds
                    distance[stops[j].Id, stops[i].Id] = distance[stops[i].Id, stops[j].Id];
                }
            }
        }
        
    }
}

