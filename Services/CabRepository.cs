﻿using Microsoft.EntityFrameworkCore;


namespace Kore.Services { 
    public class CabRepository : ICabRepository {
        
        public async Task<Cab> GetCabByIdAsync(int id) {
            var _db = new KoreContext();
            return await (from cab in _db.Cabs where cab.Id == id select cab)
            .Include(c => c.Orders)
            .SingleAsync();
        }

        public async Task<Cab> GetPureCabByIdAsync(int id) {
            var _db = new KoreContext();
            return await _db.Cabs.SingleAsync(b => b.Id == id);
        }

        public async Task<Cab> AddCabAsync(Cab cab) {
            var _db = new KoreContext();
            _db.Cabs.Add(cab); //_db.Add<Cab>(cab);
            await _db.SaveChangesAsync();
            return cab;
        }

        public async Task<Cab> ModifyCabAsync(Cab cab) {
            var _db = new KoreContext();
            _db.Cabs.Update(cab);
            await _db.SaveChangesAsync();
            return cab;
        }
    }
}
