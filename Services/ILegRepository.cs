﻿namespace Kore.Services
{
    public interface ILegRepository
    {
        public Task<Leg?> GetLegByIdAsync(int id);
        public Task<Leg> ModifyLegAsync(Leg route);
    }
}
