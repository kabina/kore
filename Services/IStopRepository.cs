﻿namespace Kore.Services
{
    public interface IStopRepository
    {
        public List<Stop> GetAllStops();
    }
}
