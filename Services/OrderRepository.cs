﻿using Microsoft.EntityFrameworkCore;

namespace Kore.Services
{
    public class OrderRepository : IOrderRepository
    {
        private const int MAX_TRIP = 10; // TODO: config file
      
        public async Task<List<Order>> GetOrderByCustomerAndStatusAsync(int id, string status) {
            var _db = new KoreContext();
            return await _db.Orders.Where(ord => ord.Customer.Id == id &&
                                ord.Status.Equals(status))
                            .Include(o => o.Cab)
                            .Include(o => o.Customer)
                            .Include(o => o.Route)
                            .Include(o => o.Leg)
                            .ToListAsync();
        }

        public async Task<Order?> GetOrderByIdAsync(int id) {
            var _db = new KoreContext();
            return await _db.Orders
                        .Include(o => o.Cab)
                        .Include(o => o.Customer)
                        .Include(o => o.Route)
                        .Include(o => o.Leg)
                        .SingleOrDefaultAsync(b => b.Id == id);
        }

        public async Task<Order?> AddOrderAsync(Order order, Customer cust) {
            var _db = new KoreContext();
            if (order.FromStand == order.ToStand)
            { // no jokes here
                return null;
            }
            Order newOrder = new Order();
            newOrder.FromStand = order.FromStand;
            newOrder.ToStand = order.ToStand;
            newOrder.MaxLoss = order.MaxLoss;
            newOrder.MaxWait = order.MaxWait;
            newOrder.Shared = order.Shared;
            newOrder.inPool = false; // set when assigned
            newOrder.Eta = -1; // set when assigned
            newOrder.Status = OrderStatus.RECEIVED;
            newOrder.Received = DateTime.Now;
            newOrder.Distance = Utils.distance[order.FromStand, order.ToStand];
            if (newOrder.Distance > MAX_TRIP)
            { // especially during rush hours
                return null;
            }
            _db.Orders.Add(newOrder);
            await _db.SaveChangesAsync();
            newOrder.Customer = cust;
            _db.Orders.Update(newOrder);
            await _db.SaveChangesAsync();
            return newOrder;
        }

        public async Task<Order> ModifyOrderAsync(Order order) {
            var _db = new KoreContext();
            _db.Orders.Update(order);
            await _db.SaveChangesAsync();
            return order;
            
        }
    }
}
