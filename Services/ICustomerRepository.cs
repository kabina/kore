﻿namespace Kore.Services
{
    public interface ICustomerRepository {
        public Task<Customer> GetCustomerByIdAsync(string id);
        public Task<Customer> GetPureCustomerByIdAsync(string id);
    }
}
