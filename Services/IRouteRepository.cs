﻿namespace Kore.Services
{
    public interface IRouteRepository
    {
        public Task<Route?> GetRouteByCabAsync(string id);
        public Task<Route?> GetRouteByIdAsync(int id);
        public Task<Route> ModifyRouteAsync(Route route);
    }
}
