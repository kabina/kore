using System.Text.Json.Serialization;
using Kore;
using Kore.Services;
using Microsoft.AspNetCore.Server.Kestrel.Core;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddHttpContextAccessor();
builder.Services.AddSingleton<ICabRepository, CabRepository>();
builder.Services.AddSingleton<IStopRepository, StopRepository>();
builder.Services.AddSingleton<IRouteRepository, RouteRepository>();
builder.Services.AddSingleton<ILegRepository, LegRepository>();
builder.Services.AddSingleton<IOrderRepository, OrderRepository>();
builder.Services.AddSingleton<ICustomerRepository, CustomerRepository>();
builder.Services.AddControllers().AddJsonOptions(x =>
                x.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles); ;
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddAuthentication("Basic")
  .AddScheme<BasicAuthenticationOptions, BasicAuthenticationHandler>("Basic", options => { });
builder.Services.AddScoped<KoreContext>(provider => provider.GetService<KoreContext>());
builder.Logging.ClearProviders();
builder.Logging.AddConsole();
builder.Logging.AddFile("log.txt"); // $@"{Directory.GetCurrentDirectory()}/log.txt"

//builder.WebHost.UseStartup<Startup>();

var app = builder.Build();

// Configure the HTTP request pipeline.
//if (app.Environment.IsDevelopment()) {
//    app.UseSwagger();
//    app.UseSwaggerUI();
//}

app.UseHttpsRedirection();
app.UseAuthentication();
app.UseAuthorization();
app.MapControllers();
app.Run();
