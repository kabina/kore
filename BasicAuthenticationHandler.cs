﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Options;
using System.Security.Claims;
using System.Text;
using System.Text.Encodings.Web;

namespace Kore
{
    public class BasicAuthenticationHandler : AuthenticationHandler<BasicAuthenticationOptions>
    {
        public BasicAuthenticationHandler(
            IOptionsMonitor<BasicAuthenticationOptions> options,
            ILoggerFactory logger,
            UrlEncoder encoder,
            ISystemClock clock
            ) : base(options, logger, encoder, clock)
        {
        }

        protected override Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            var authHeader = Request.Headers["Authorization"].ToString();
            if (authHeader != null && authHeader.StartsWith("basic", StringComparison.OrdinalIgnoreCase))
            {
                var token = authHeader.Substring("Basic ".Length).Trim();
                System.Console.WriteLine(token);
                var credentialstring = Encoding.UTF8.GetString(Convert.FromBase64String(token));
                var credentials = credentialstring.Split(':');
                if (credentials[0].StartsWith("adm") || credentials[0].StartsWith("cus") 
                            || credentials[0].StartsWith("cab")) { // credentials[1] -> password
                    Claim role = null, username=null, userid=null;
                    string name = credentials[0].Substring(0,3);
                    switch (name) {
                        case "adm": role = new Claim(ClaimTypes.Role, "Admin");
                                    username = new Claim("username", credentials[0]);
                                    userid = new Claim("userid", credentials[0].Substring(3, credentials[0].Length - 3));  
                                    break;
                        case "cab": role = new Claim(ClaimTypes.Role, "Cab");
                                    username = new Claim("username", credentials[0]);
                                    userid = new Claim("userid", credentials[0].Substring(3, credentials[0].Length - 3));
                                    break;
                        case "cus": role = new Claim(ClaimTypes.Role, "Customer");
                                    username = new Claim("username", credentials[0]);
                                    userid = new Claim("userid", credentials[0].Substring(4, credentials[0].Length - 4));
                                    break;
                    }
                    var identity = new ClaimsIdentity(new[] { username, userid, role }, "Basic");
                    var claimsPrincipal = new ClaimsPrincipal(identity);
                    return Task.FromResult(AuthenticateResult.Success(new AuthenticationTicket(claimsPrincipal, Scheme.Name)));
                }
                Response.StatusCode = 401;
                Response.Headers.Add("WWW-Authenticate", "Basic realm=\"kabina.org\"");
                return Task.FromResult(AuthenticateResult.Fail("Invalid Authorization Header"));
            }
            else {
                Response.StatusCode = 401;
                Response.Headers.Add("WWW-Authenticate", "Basic realm=\"kabina.org\"");
                return Task.FromResult(AuthenticateResult.Fail("Invalid Authorization Header"));
            }
        }
    }
}
